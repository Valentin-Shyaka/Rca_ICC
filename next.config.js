/** @type {import('next').NextConfig} */
module.exports = {
	reactStrictMode: false,
	images: {
		domains: [
			"cdn.sanity.io",
			"lh3.googleusercontent.com",
			"cdn.discordapp.com",
		],
	},
};
